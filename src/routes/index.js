const router = require('express').Router();

router.get('/', (req, res) => {
  res.render(__dirname+'/routes/index.jade', { script: config.scriptsLocation});
});

module.exports = router;
