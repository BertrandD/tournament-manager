var _ = require('lodash');
var slack = require('@slack/client');
var config = require('./config');
var slackClient = new slack.WebClient(config.slack.token, console.error.bind(console));

var slackUser = {
    username: 'Tournament manager',
    icon_emoji: ':bulb:'
};

function newTournament (tournament) {
    slackClient.chat.postMessage(
        config.slack.channel,
        'Hey new tournament created !',
        slackUser
    );
}

function updateTournament (tournament) {
    slackClient.chat.postMessage(
        config.slack.channel,
        'Hey tournament '+ tournament.name +' updated !',
        slackUser
    );
}

function sendMessage (message) {
    slackClient.chat.postMessage(
        config.slack.channel,
        message,
        slackUser
    );
}

module.exports = {
    newTournament: newTournament,
    updateTournament: updateTournament
};